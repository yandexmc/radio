package com.uliana.girlradio;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class PlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener {

    private static final String CACHE_FILE_NAME = "radio.mp3";
    private static final int NOTIFY_ID = 1;

    private final IBinder musicBind = new MusicBinder();
    private MediaPlayer player;
    private String streamPath;
    private File cacheFile;
    private boolean isSavingProcessStarted;
    private boolean isFileExist;
    private SaveToFileAsyncTask saveToFileAsyncTask;
    private ChangeServiceStateListener changePlayerStateListener;

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("MUSIC SERVICE", "onBind");
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("MUSIC SERVICE", "onUnbind");
        if (!isSavingProcessStarted) {
            player.stop();
            player.release();
            stopForeground(true);
        }
        if (saveToFileAsyncTask != null) {
            saveToFileAsyncTask.cancel(true);
        }
        return false;
    }

    @Override
    public void onCreate() {
        Log.i("MUSIC SERVICE", "onCreate");
        cacheFile = new File(getApplicationContext().getExternalCacheDir(), CACHE_FILE_NAME);
        if (cacheFile.exists()) {
            cacheFile.delete();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("MUSIC SERVICE", "onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.start();
        if (changePlayerStateListener != null) {
            changePlayerStateListener.onStateChanged(
                    isFileExist ? PlayerServiceState.STARTED_FROM_FILE : PlayerServiceState.STARTED);
        }
        addNotification();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.i("MUSIC SERVICE", "onCompletion");
        cacheFile.delete();
        play();
    }


    public void play() {
        changePlayerStateListener.onStateChanged(PlayerServiceState.BUFFERING);
        initPlayer();
        if (isSavingProcessStarted) {
            saveToFileAsyncTask.cancel(true);
            isSavingProcessStarted = false;
        }

        try {
            isFileExist = cacheFile.exists();
            if (isFileExist) {
                Log.d("MUSIC SERVICE", "play from file");
                player.setDataSource(getApplicationContext(), Uri.fromFile(cacheFile));
            } else {
                Log.d("MUSIC SERVICE", "play from stream");
                player.setDataSource(getApplicationContext(), Uri.parse(streamPath));
            }
        } catch (Exception e) {
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        player.prepareAsync();

    }

    public void pause() {
        if (player.isPlaying()) {
            player.stop();
            stopForeground(true);
            player.release();
        }
        if (changePlayerStateListener != null) {
            changePlayerStateListener.onStateChanged(PlayerServiceState.RECORDING);
        }
        saveToFileAsyncTask = new SaveToFileAsyncTask();
        saveToFileAsyncTask.execute();
    }

    public void setStream(String streamPathUrl) {
        streamPath = streamPathUrl;
    }

    public void setChangePlayerStateListener(ChangeServiceStateListener changePlayerStateListener) {
        this.changePlayerStateListener = changePlayerStateListener;
    }

    public boolean isPlaying() {
        return player != null && player.isPlaying();
    }


    private void initPlayer() {
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        player.setOnPreparedListener(this);
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
    }

    private void addNotification() {
        Intent notIntent = new Intent(this, MainActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);

        builder.setContentIntent(pendInt)
                .setSmallIcon(R.drawable.ic_play)
                .setOngoing(true)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(streamPath);
        Notification not = builder.build();

        startForeground(NOTIFY_ID, not);
    }


    public class MusicBinder extends Binder {
        PlayerService getService() {
            return PlayerService.this;
        }
    }

    private class SaveToFileAsyncTask extends AsyncTask<String, Void, Void> {
        FileOutputStream fileOutputStream;

        protected Void doInBackground(String... urls) {
            Log.d("MUSIC SERVICE", "start recording");
            isSavingProcessStarted = true;
            URL url;
            try {
                if (!cacheFile.exists()) {
                    cacheFile.createNewFile();
                }
                url = new URL(streamPath);
                InputStream inputStream = url.openStream();
                fileOutputStream = new FileOutputStream(cacheFile);

                int c;
                while ((c = inputStream.read()) != -1) {
                    fileOutputStream.write(c);
                }
            } catch (IOException e) {
                Log.d("MUSIC SERVICE", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onCancelled(Void aVoid) {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                Log.d("MUSIC SERVICE", e.getMessage());
            }
        }
    }

    public interface ChangeServiceStateListener {

        void onStateChanged(PlayerServiceState state);

    }

}