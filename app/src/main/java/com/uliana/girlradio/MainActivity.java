package com.uliana.girlradio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity implements PlayerService.ChangeServiceStateListener {
    private static final String RADIO_STREAM_URL = "http://bookradio.hostingradio.ru:8069/fm";

    @BindView(R.id.btn_play)
    View btnPlay;

    @BindView(R.id.btn_pause)
    View btnPause;

    @BindView(R.id.tv_state)
    TextView textViewState;


    private PlayerService streamAudioService;
    private Intent playIntent;
    private boolean streamBound = false;

    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            PlayerService.MusicBinder binder = (PlayerService.MusicBinder) service;
            streamAudioService = binder.getService();
            streamAudioService.setStream(RADIO_STREAM_URL);
            streamBound = true;
            streamAudioService.setChangePlayerStateListener(MainActivity.this);
            if (streamAudioService.isPlaying()) {
                btnPlay.setVisibility(View.GONE);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            streamBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(this, PlayerService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }

    @OnClick(R.id.btn_play)
    public void onPlayClicked() {
        streamAudioService.play();
    }

    @OnClick(R.id.btn_pause)
    public void onPauseClicked() {
        streamAudioService.pause();
    }

    @Override
    protected void onDestroy() {
        if (musicConnection != null) {
            unbindService(musicConnection);
        }
        stopService(playIntent);
        streamAudioService = null;
        super.onDestroy();
    }

    @Override
    public void onStateChanged(PlayerServiceState state) {
        switch (state) {
            case STARTED: {
                btnPlay.setVisibility(View.GONE);
                textViewState.setText(R.string.state_stream);
                break;
            }
            case STARTED_FROM_FILE: {
                btnPlay.setVisibility(View.GONE);
                textViewState.setText(R.string.state_memory);
                break;
            }
            case RECORDING: {
                btnPlay.setVisibility(View.VISIBLE);
                textViewState.setText(R.string.state_recording);
                break;
            }
            case BUFFERING: {
                btnPlay.setVisibility(View.VISIBLE);
                textViewState.setText(R.string.state_buffering);
                break;
            }

        }
    }
}
