package com.uliana.girlradio;

public enum PlayerServiceState {
    STARTED,
    STARTED_FROM_FILE,
    STOPPED,
    BUFFERING,
    RECORDING
}
